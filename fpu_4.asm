;%include "io.inc"
extern printf

section .data 
          
            formato db "%.2f", 10, 0
            result dq 0.0
            
section .text

global suma_vf

suma_vf:

            push ebp
            mov ebp, esp
 
            mov eax,[ebp+8]
            mov ecx,[ebp+12]
            
            fld dword [eax]         
ciclo:                                  
            add eax,4
            fld dword [eax]  
            fadd
            dec ecx
            jnz ciclo
 
                                             
            fst  qword[result]
            push dword[result+4]
            push dword[result]
            push formato
            
            call printf
            add esp, 12
                                  
                                                        
            mov esp, ebp
            pop ebp                                   
                                                                                                           
            ret

