;%include "io.inc"
extern printf

section .data 
          
            formato db "%f", 10, 0
             result dq 0.0                  
            result1 dq 0.0
            raiz1    dq 0.0 
            raiz2    dq 0.0          
                                       
             cuatro dd 4.0
             dos    dd 2.0            
               
                     
section .text

global CMAIN

CMAIN:
;------------------------------------------
             push ebp
             mov ebp, esp
             
             fld  dword [ebp + 8] ; cargo a
             fld  dword [ebp + 16]; cargo b
             fmul
             fld  dword [cuatro] 
             fmul st0,st1        ; resultado 4ac
             fchs                ; cambio signo -(4ac)            
             fld  dword [ebp + 12]; cargo b
             fmul st0, st0       ; b^2
             fadd                ; b^2 + (-4ac)   
             fsqrt               ; sqrt( b^2 + (-4ac))
             fchs                ; cambio signo ***
             fld  dword [ebp + 12]; cargo b (estoy en numerador)
             fchs                ; cambio a -b 
             fadd
             fst qword[result1]  ;guardo -b + sqrt(b^2 - 4ac)
             fld  dword [ebp + 8] ; cargo a
             fld  dword [dos]
             fmul                 ; resultado 2a
             fld qword[result1]
             fdiv st0,st1                          
             fst qword[raiz1]


             push dword[raiz1+4]
             push dword[raiz1]
             push formato
            
             call printf
             add esp, 12           
            
;------------------------------------------

             finit
             fld  dword [ebp + 8] ; cargo a
             fld  dword [ebp + 16]; cargo b
             fmul
             fld  dword [cuatro] 
             fmul st0,st1        ; resultado 4ac
             fchs                ; cambio signo -(4ac)            
             fld  dword [ebp + 12]; cargo b
             fmul st0, st0       ; b^2
             fadd                ; b^2 + (-4ac)   
             fsqrt               ; sqrt( b^2 + (-4ac))
             fld  dword [ebp + 12]; cargo b (estoy en numerador)
             fchs                ; cambio a -b 
             fadd
             fst  qword[result1]  ;guardo -b + sqrt(b^2 - 4ac)
             fld  dword [ebp + 8] ; cargo a
             fld  dword [dos]
             fmul                 ; resultado 2a
             fld qword[result1]
             fdiv st0,st1                          
             fst qword[raiz2]


            push dword[raiz2+4]
            push dword[raiz2]
            push formato
            call printf
            add esp, 12
            
                       
            mov esp, ebp
            pop ebp
 ;-----------------------------------
 
            
            ret

