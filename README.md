# OCII - TP1

**TRABAJO PRACTICO DE LA MATERIA ORGANIZACIÓN DEL COMPUTADOR II - UNGS**
=================================================================

Resolvente en ASM
==================


**- Implmentación de la Fórmula resolvente y llamada desde C (Puntos 1,2 y 3)**

El cálculo se realiza mediante un programa realizado en NASM que espera le pasen los parámetros a, b y c. El nombre del archivo es:   **resolv.asm**

El progama en NASM es llamado desde un programa en C que pide al usuario los tres parámetros. El nombre del archivo es:  **resolv.c**


Para hacer sencilla la compilación de ambos archivos de código y su posterior ejecución se ha realizado un script, el mismo se llama:   **script.sh**

A continuación se puede ver la salida del programa en C luego de compiar y linkear los archivos:

![Scheme](/captura.jpg)


TODOS LOS ARCHIVOS NOMBRADOS SE ENCUENTRAN EN ESTE REPOSITORIO.


**- Ejercicios Obligatorios**

Se ha subido un archivo pdf con los siguientes ejercicios obligatorios:

○ Ejercicio 4 - Gestión de memoria.

○ Ejercicio 6 - Gestión de memoria.

○ Ejercicio 7 - Gestión de memoria.

○ Ejercicio 4 - FPU.

Adicionalmente para el caso del ejercicio  de FPU se ha subido el codigo en NASM que realiza el calculo de punto flotante y la función invocadora en C. Los nombres de estos archivos son:

○  fpu_4.c

○  fpu_4.asm

siguiendo el modo de trabajo del punto anterior está disponible el script que permite compilar, linkear los archivos y luego ejecutarlos. El nombre sel script es: 

○  fpu.sh


A continuación se puede ver la salida del programa en C luego de compiar y linkear los archivos:

![Scheme](/fpu.jpg)








