#include <stdio.h>
#include <stdlib.h>

extern void CMAIN(float a, float b, float c);

int main (){

    float a;
    float b;
    float c; 

    printf(" Ingrese valor de a: ");
    scanf("%f", &a);
    printf(" Ingrese valor de b: ");
    scanf("%f", &b);
    printf(" Ingrese valor de c: ");
    scanf("%f", &c);
    printf("\n");
    printf(" Las raices de la resolvente son:");
    printf("\n");

    CMAIN(a,b,c);
	
	return 0;
}


